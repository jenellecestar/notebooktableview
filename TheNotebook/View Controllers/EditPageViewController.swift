//
//  EditPageViewController.swift
//  TheNotebook
//
//  Created by MacStudent on 2018-07-18.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CoreData

class EditPageViewController: UIViewController {

    // OUTLETS:
    @IBOutlet weak var textview: UITextView!
    
    // SEGUE: Data from previous screen
    var page:Page!
    
    // CORE DATA VARIABLE:
    var myContext:NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("I'm on the edit page screen!")
        
        // SEGUE DEBUG NONSENSE:
        print("++++++")
        print("The other screen sent me: ")
        print(page.text)
        print("++++++")
        
        // UI: update the user interface
        textview.text = page.text!
        
        
        // COREDATA --------
        // app delegate
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        // set the context
        myContext = appDelegate.persistentContainer.viewContext
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        // get the stuff in the textview
        let y  = textview.text!
        
        // update your page object
        self.page.text = y
        
        // make the do/try/catch block
        do {
            // save the page!
            try myContext.save()
            print("saved!")
        }
        catch {
            print("error updating the page")
        }
        
    }
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
